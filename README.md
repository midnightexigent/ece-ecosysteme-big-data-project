# Observability pipline for logs with HBase, Kafka and Vector

## Note

**The following components were developped as part of this project. Feel free to check them out**

- [vector-http-hbase](https://github.com/midnightexigent/vector-http-sink-hbase-rs): to make the bridge between
`vector` and `hbase`
- [hbase-thrift](https://github.com/midnightexigent/hbase-thrift-rs): client for HBase in Rust based on its `thrift` API
- [thrift-pool](https://github.com/midnightexigent/thrift-pool-rs): to easily implement Connection Pools for ANY
`thrift` client in Rust. This was extracted from `hbase-thrift` 

These were developped because we wanted to show a how to use `vector` to glue together 
different sources of logs to their destination(s). But `vector` doesn't support writing data to `HBase` (likely because
it is written in Rust and there no clients for `HBase` in the Rust programming language). However, `vector` 
can send data to any http server, and that http server can then route the data to HBase. The http server 
could have been written in any programming language (easiest would've been Go or Java) but we made it Rust. So 
now, there is a client library for HBase in Rust which could be used for various purposes, including making `vector` 
natively support `HBase` as a sink

<br>

## Description

In cloud application, it is important to know what's happening in the different services
as well as their internal states. Observability pipelines allow us to do that 
(see [article](https://www.splunk.com/en_us/data-insider/what-is-observability.html))

However, observability pipelines are usually difficult to put in place. They need to 
be fault-tolerant, highly available and highly scalable. Cloud applications are (usually) either 
clusters of VMs (Virtual machines) or containers managed by an 
orchestrator (like kubernetes). Every layer of a cloud 
infrastructur can generate data (logs, metrics, traces) that are relevant to enhance the
observability of the system. This article shows how to 
set up such a pipeline for `logs` using the following tech-stack

- [Kafka](https://kafka.apache.org/) to broker logs from N sources to 1 long-term storage 
- [HBase](https://hbase.apache.org/) to store the logs as structured data
- [Vector](https://vector.dev/) as the glue

So here is the target architecture: 

![architecture](docs/architecture.jpg)

In this architecture, a `vector` component is installed on every 
source to collect its logs. They are then put into structured
data (if possible) and sent in a `kafka` topic
which will contain all the logs of all the sources in the cluster.
That topic is then consumed by another `vector` component which can optionnaly
do other computations on the logs (such as generating metrics, sampling, etc.),
but most importantly, forward the structured logs in `hbase`.
If we look closely at the [sinks](https://vector.dev/docs/reference/configuration/sinks/)
supported by `vector`, there is no `HBase`. That's why we provide an 
[adaptor](https://github.com/midnightexigent/vector-http-sink-hbase-rs) that 
takes data from the [http sink](https://vector.dev/docs/reference/configuration/sinks/http/)
and sends it to `HBase`'s `thrift` endpoint

Let's get started. Before proceeding, read the documentations of the cited components.
More specifically, see the different
[example configs for `vector`](https://github.com/vectordotdev/vector/tree/master/config/examples)


## Example setup

Below are some sample configs to get you started. 

### Requirements

This tutorial assumes that there are a working `Kafka` and `HBase` clusters.

The fastest way to do it is with `docker`

```bash
docker run --name kafka -p 9092:9092 -d bashj79/kafka-kraft # start kafka
docker run --name hbase -p 9090:9090 -d dajobe/hbase # start hbase

```

This tutorial also assumes that vector is in $PATH, 
see its [download](https://vector.dev/download/) section 
or [installation script](https://vector.dev/docs/setup/installation/manual/vector-installer/)

The [vector-http-sink-hbase](https://github.com/midnightexigent/vector-http-sink-hbase-rs)
extension is also needed to write to `HBase`. See its installation instructions

### Collecting logs with vector and sending them to kafka

Let's tell `vector` to take some `logs`, parse them, and send them to a kafka topic.
In a real scenario, this should be replicated accross all the nodes where 
logs should be collected.

```bash
vector --config vector-collector.toml --verbose

```

And the config can look something like this. Of course, other sources 
and transforms can be added and captured by the sink. 
This example aims to be minimal so we only have these

```toml
# vector-collector.toml

# Let's make vector collect its own logs
[sources.internal_logs]
type = "internal_logs"

# Random Syslog-formatted logs
[sources.dummy_logs]
type = "demo_logs"
format = "syslog"
interval = 1


# Parse Syslog logs
# See the Vector Remap Language reference for more info: https://vrl.dev
[transforms.parse_logs]
type = "remap"
inputs = ["dummy_logs"]
source = '''
. = parse_syslog!(string!(.message))
'''

# Now we can capture the structured logs and send it to kafka
[sinks.publish_kafka]
type = "kafka"
inputs = ["parse_logs", "internal_logs"]
encoding.codec = "json"
bootstrap_servers = "localhost:9092"
topic = "logs"

```

### Persisting logs

Now we can consume the logs and store them in HBase. That is done
in 3 steps: kafka -> vector -> adaptor -> hbase. The adaptor is the 
afromentioned component that makes it possible to write data 
from vector to HBase. We could also use kafka connect to directy
send data to HBase but that doesn't allow us to make additionnal 
processing (like filtering, enriching, the data,
generating metrics, etc.) with Vector when consuming the logs

First, connect to `hbase` and create the table

```bash
docker run --rm -it --link hbase:hbase-docker dajobe/hbase hbase shell
create 'logs','data'

```

Then, start `vector-http-sink-hbase`.
Its default command line params are fine

```bash
RUST_LOG=trace vector-http-sink-hbase # defaults are fine

```

Finally, start the `vector` component that will consume from kafka 
and push `logs` into `vector-http-sink-hbase`

```bash
vector --config vector-consumer.toml
```

The config can look something like this. Of course, we can have 
intermediate processing on the consumed logs, but this 
tutorial aims to be minimal

```toml
# vector-consumer.toml

[sources.kafka_logs]
type = "kafka"
bootstrap_servers = "localhost:9092"
decoding.codec = "json"
group_id = "vector"
topics = [ "logs" ]


# in case logs are nested
[transforms.flatten_logs]
type = "remap"
inputs = ["kafka_logs"]
source = """
. = flatten(.)
"""

[sinks.http_logs]
type = "http"
inputs = [
  "flatten_logs"
]
encoding.codec = "json"
uri = "localhost:3000"

```

### Accessing the data

Now, if we reopen a shell to hbase, we should see the logs

```bash
docker run --rm -it --link hbase:hbase-docker dajobe/hbase hbase shell
scan 'logs'


```


